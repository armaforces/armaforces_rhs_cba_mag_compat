class CfgMagazineWells
{
    class CBA_338LM_T5000
    {
        RHS_AFRF_Mags[] =
        {
            //5 round T-5000 magazine
            "rhs_5Rnd_338lapua_t5000"
        };
    };
    class CBA_40mm_GP
    {
        RHS_AFRF_grenades[] =
        {
            "rhs_VOG25",
            "rhs_VOG25p",
            "rhs_vg40tb",
            "rhs_vg40sz",
            "rhs_vg40op_white",
            "rhs_vg40op_green",
            "rhs_vg40op_red",
            "rhs_GRD40_white",
            "rhs_GRD40_green",
            "rhs_GRD40_red",
            "rhs_VG40MD_White",
            "rhs_VG40MD_Green",
            "rhs_VG40MD_Red",
            "rhs_GDM40"
        };
    };
    class CBA_545x39_AK
    {
        RHS_AFRF_mags[] =
        {
            //7N6 (6L20 "only", for now)
            "rhs_30Rnd_545x39_AK",
            "rhs_30Rnd_545x39_7N6_AK",
            "rhs_30Rnd_545x39_AK_no_tracers",//Depreciated
            "rhs_30Rnd_545x39_7N6_green_AK",

            //7T3 (Tracer) (6L20 and Plum 6L23)
            "rhs_30Rnd_545x39_AK_green",
            "rhs_30Rnd_545x39_AK_plum_green",

            //7U1 (Subsonic) (6L23 "only", for now)
            "rhs_30Rnd_545x39_7U1_AK",

            //7N6M (6L20 & Plum 6L23)
            "rhs_30Rnd_545x39_7N6M_AK",
            "rhs_30Rnd_545x39_7N6M_green_AK",
            "rhs_30Rnd_545x39_7N6M_plum_AK",

            //7N10 (6L23 "only", for now)
            "rhs_30Rnd_545x39_7N10_AK",
            "rhs_30Rnd_545x39_7N10_plum_AK",
            "rhs_30Rnd_545x39_7N10_camo_AK",
            "rhs_30Rnd_545x39_7N10_desert_AK",

            //7N22 (6L23 "only", for now)
            "rhs_30Rnd_545x39_7N22_AK",
            "rhs_30Rnd_545x39_7N22_plum_AK",
            "rhs_30Rnd_545x39_7N22_camo_AK",
            "rhs_30Rnd_545x39_7N22_desert_AK",

            //Jungle-Taped 6L23 (only "7N10", for now)
            "rhs_30Rnd_545x39_7N10_2mag_AK",
            "rhs_30Rnd_545x39_7N10_2mag_plum_AK",
            "rhs_30Rnd_545x39_7N10_2mag_camo_AK",
            "rhs_30Rnd_545x39_7N10_2mag_desert_AK",

            //6L18 (Orange Bakelite RPK-74)
            "rhs_45Rnd_545x39_AK",
            "rhs_45Rnd_545x39_7N6_AK",
            "rhs_45Rnd_545x39_7N10_AK",
            "rhs_45Rnd_545x39_7N22_AK",
            "rhs_45Rnd_545x39_AK_green",
            "rhs_45Rnd_545x39_7U1_AK"

        };

    };
    class CBA_762x39_AK
    {
        RHS_AFRF_mags[] =
        {
            //AK-47/AKM Steel mags
            "rhs_30Rnd_762x39mm",
            "rhs_30Rnd_762x39mm_tracer",
            "rhs_30Rnd_762x39mm_89",
            "rhs_30Rnd_762x39mm_U",
            //AKM Bakelite
            "rhs_30Rnd_762x39mm_bakelite",
            "rhs_30Rnd_762x39mm_bakelite_tracer",
            "rhs_30Rnd_762x39mm_bakelite_89",
            "rhs_30Rnd_762x39mm_bakelite_U",
            //AK-103 Polymer
            "rhs_30Rnd_762x39mm_polymer",
            "rhs_30Rnd_762x39mm_polymer_tracer",
            "rhs_30Rnd_762x39mm_polymer_89",
            "rhs_30Rnd_762x39mm_polymer_U",
            //10rnd AK-103 Polymer
            "rhs_10Rnd_762x39mm",
            "rhs_10Rnd_762x39mm_tracer",
            "rhs_10Rnd_762x39mm_89",
            "rhs_10Rnd_762x39mm_U"
        };
    };
    class CBA_762x39_RPK
    {
        RHS_AFRF_mags[] =
        {
            //75rnd RPK Drum
            "rhs_75Rnd_762x39mm",
            "rhs_75Rnd_762x39mm_tracer",
            "rhs_75Rnd_762x39mm_89"
        };
    };
    class CBA_762x54R_LINKS
    {
        RHS_AFRF_belts[] =
        {
            "rhs_100Rnd_762x54mmR",
            "rhs_100Rnd_762x54mmR_green",
            "rhs_100Rnd_762x54mmR_7N13",
            "rhs_100Rnd_762x54mmR_7N26",
            "rhs_100Rnd_762x54mmR_7BZ3"
        };
    };
    class CBA_762x54R_SVD
    {
        RHS_AFRF_mags[] =
        {
            "rhs_10Rnd_762x54mmR_7N1",
            "rhs_10Rnd_762x54mmR_7N14"
        };
    };
    class CBA_9x18_PM
    {
        RHS_AFRF_Mags[] =
        {
            //8 round Makarov PM magazine
            "rhs_mag_9x18_8_57N181S"
        };
    };
    class CBA_9x19_MP443
    {
        RHS_AFRF_Mags[] =
        {
            //17 round MP-443 magazine
            "rhs_mag_9x19_17"
        };
    };
    class CBA_9x19_PP2000
    {
        RHS_AFRF_Mags[] =
        {
            //20 round PP-2000 magazines
            "rhs_mag_9x19mm_7n21_20",
            "rhs_mag_9x19mm_7n31_20",
            //44 round PP-2000 magazines
            "rhs_mag_9x19mm_7n21_44",
            "rhs_mag_9x19mm_7n31_44"
        };
    };
    class CBA_9x21_SR1M
    {
        RHS_AFRF_Mags[] = 
        {
            //18 round 6P53 magazines
            "rhs_18rnd_9x21mm_7BT3",
            "rhs_18rnd_9x21mm_7N28",
            "rhs_18rnd_9x21mm_7N29"
        };
    };
    class CBA_9x39_VSS
    {
        RHS_AFRF_mags[] =
        {
            "rhs_20rnd_9x39mm_SP5",
            "rhs_20rnd_9x39mm_SP6",
            "rhs_10rnd_9x39mm_SP5",
            "rhs_10rnd_9x39mm_SP6"
        };
    };
    class CBA_RPG7
    {
        RHS_AFRF_rockets[] =
        {
            "rhs_rpg7_PG7V_mag",
            //"rhs_rpg7_PG7VS_mag",
            //"rhs_rpg7_PG7VM_mag",
            "rhs_rpg7_PG7VL_mag",
            "rhs_rpg7_PG7VR_mag",
            "rhs_rpg7_OG7V_mag",
            "rhs_rpg7_TBG7V_mag",
            "rhs_rpg7_TYPE69_airburst_mag"
        };
    };
};
