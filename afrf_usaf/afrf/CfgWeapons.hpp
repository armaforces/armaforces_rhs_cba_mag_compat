class CfgWeapons
{
    class Rifle;
    class Rifle_Base_F;
    class Pistol_Base_F;

    //Assault Rifles
    //AK-74, AK-105 (5,45x39)
    class rhs_weap_ak74m_Base_F: Rifle_Base_F
    {
        magazineWell[] += {"CBA_545x39_AK","CBA_545x39_RPK"};
    };
    class rhs_weap_ak74m;
    //AKM, AK-103, AK-104 (7,62x39)
    class rhs_weap_akm : rhs_weap_ak74m
    {
        magazineWell[] += {"CBA_762x39_AK","CBA_762x39_RPK"};
    };
    //AS-VAL, VSS Vintorez
    class rhs_weap_asval: rhs_weap_ak74m
    {
        magazineWell[] += {"CBA_9x39_VSS"};
    };

    //LMGs
    //PKM, PKP
    class Rifle_Long_Base_F;
    class rhs_pkp_base: Rifle_Long_Base_F
    {
        magazineWell[] += {"CBA_762x54R_LINKS"};
    };

    //Sniper rifles
    //SVD
    class rhs_weap_svd: rhs_weap_ak74m
    {
        magazineWell[] += {"CBA_762x54R_SVD"};
    };
    //T-5000
    class rhs_weap_orsis_Base_F: Rifle_Base_F
    {
        magazineWell[] += {"CBA_338LM_T5000"};
    };

    //Handguns
    //PP-2000, PP-2000 (Folded)
    class hgun_PDW2000_F;
    class rhs_weap_pp2000: hgun_PDW2000_F
    {
        magazineWell[] += {"CBA_9x19_PP2000"};
    };
    //MP-443
    class hgun_Rook40_F;
    class rhs_weap_pya: hgun_Rook40_F
    {
        magazineWell[] += {"CBA_9x19_MP443"};
    };
    //Makarov PM
    class rhs_weap_makarov_pm: rhs_weap_pya
    {
        magazineWell[] += {"CBA_9x18_PM"};
    };
    //6P53
    class rhs_weap_6p53: rhs_weap_makarov_pm
    {
        magazineWell[] += {"CBA_9x21_SR1M"};
    };

    //RPG-7
    class Launcher_Base_F;
    class rhs_weap_rpg7: Launcher_Base_F
    {
        magazineWell[] += {"CBA_RPG7"};
    };
};
