class CfgPatches
{
    class armaforces_rhs_cba_mag_compat_afrf
    {
        name = "ArmaForces - RHS CBA Mag Compat - AFRF";
        units[] = {};
        weapons[] = {};
        requiredVersion = 0.1;
        requiredAddons[] = {"rhs_c_weapons", "cba_main"};
        author = "3Mydlo3, veteran29";
    };
};

#include "CfgMagazineWells.hpp"
#include "CfgWeapons.hpp"
