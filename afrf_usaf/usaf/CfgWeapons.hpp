class CfgWeapons
{
    class Rifle;
    class Rifle_Base_F;
    class Pistol_Base_F;
    class Launcher_Base_F;

    //M4, HK416, M16 etc.
    class arifle_MX_Base_F;
    class rhs_weap_m4_Base: arifle_MX_Base_F
    {
        magazineWell[] += {"CBA_556x45_STANAG","CBA_556x45_STANAG_2D"};
    };

    //Sniper rifles
    //M14
    class srifle_EBR_F;
    class rhs_weap_m14ebrri: srifle_EBR_F
    {
        magazineWell[] += {"CBA_762x51_M14"};
    };
    //Mk11
    class rhs_weap_sr25: rhs_weap_m14ebrri
    {
        magazineWell[] += {"CBA_762x51_AR10"};
    };
    //M2010
    class rhs_weap_XM2010_Base_F: Rifle_Base_F
    {
        magazineWell[] += {"CBA_300WM_AICS"};
    };
    //M24
    class rhs_weap_m24sws: rhs_weap_XM2010_Base_F
    {
        magazineWell[] += {"CBA_762x51_5rnds"};
    };
    //M40A5
    class rhs_weap_m40a5: rhs_weap_XM2010_Base_F
    {
        magazineWell[] += {"CBA_762x51_AICS"};
    };
    //M107
    class GM6_base_F;
    class rhs_weap_M107_Base_F: GM6_base_F
    {
        magazineWell[] += {"CBA_50BMG_M107"};
    };

    //LMGs
    //M249
    class rhs_weap_saw_base: Rifle_Base_F
    {
        magazineWell[] += {"CBA_556x45_MINIMI","CBA_556x45_STANAG","CBA_556x45_STANAG_2D"}
    };
    //M240
    class rhs_weap_M249_base;
    class rhs_weap_m240_base: rhs_weap_M249_base
    {
        magazineWell[] += {"CBA_762x51_LINKS"};
    };

    //Shotguns
    //M590 Short
    class rhs_weap_M590_5RD: Rifle_Base_F
    {
        magazineWell[] += {"CBA_12g_1rnd","CBA_12g_2rnds","CBA_12g_3rnds","CBA_12g_4rnds","CBA_12g_5rnds"};
    };
    //M590 Long
    class rhs_weap_M590_8RD: rhs_weap_M590_5RD
    {
        magazineWell[] += {"CBA_12g_1rnd","CBA_12g_2rnds","CBA_12g_3rnds","CBA_12g_4rnds","CBA_12g_5rnds","CBA_12g_6rnds","CBA_12g_7rnds","CBA_12g_8rnds"};
    };

    //MP7 SMG
    class SMG_02_base_F;
    class rhsusf_weap_MP7A1_base_f: SMG_02_base_F
    {
        magazineWell[] += {"CBA_46x30_MP7"};
    };

    //Handguns
    //M320 GL
    class rhs_weap_M320_Base_F: Pistol_Base_F
    {
        magazineWell[] += {"CBA_40mm_M203"};
    };
    //Glock 17
    class hgun_P07_F;
    class rhsusf_weap_glock17g4: hgun_P07_F
    {
        magazineWell[] += {"CBA_9x19_Glock_Full"};
    };
    //M9
    class rhsusf_weap_m9: rhsusf_weap_glock17g4
    {
        magazineWell[] += {"CBA_9x19_M9"};
    };
    //M1911
    class hgun_ACPC2_F;
    class rhsusf_weap_m1911a1: hgun_ACPC2_F
    {
        magazineWell[] += {"CBA_45ACP_1911"};
    };

    //M32 MGL
    class rhs_weap_m32_Base_F: Rifle_Base_F
    {
        magazineWell[] += {"CBA_40mm_M203","CBA_40mm_3GL","CBA_40mm_M203_6rnds"};
    };

    //MAAWS
    class rhs_weap_maaws: Launcher_Base_F
    {
        magazineWell[] += {"CBA_Carl_Gustaf"};
    };
    //SMAW
    class rhs_weap_smaw: Launcher_Base_F
    {
        magazineWell[] += {"CBA_SMAW"};
        //SMAW Spotting Rifle
        class rhs_weap_smaw_SR: Launcher_Base_F
        {
            magazineWell[] += {"CBA_SMAW_Spotting_Rifle"};
        };
    };
};
