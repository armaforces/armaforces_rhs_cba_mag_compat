class CfgPatches
{
    class armaforces_rhs_cba_mag_compat_usf
    {
        name = "ArmaForces - RHS CBA Mag Compat - USAF";
        units[] = {};
        weapons[] = {};
        requiredVersion = 0.1;
        requiredAddons[] = {"rhsusf_c_weapons", "cba_main"};
        author = "3Mydlo3, veteran29";
    };
};

#include "CfgMagazineWells.hpp"
#include "CfgWeapons.hpp"
