class CfgWeapons
{
    class Rifle;
    class Rifle_Base_F;
    class Pistol_Base_F;

    //G36
    class rhs_weap_g36_base: Rifle_Base_F
    {
        magazineWell[] += {"CBA_556x45_G36"};
    };
    //M-70, M-92 Zastava
    class rhs_weap_m70_base: Rifle_Base_F
    {
        magazineWell[] += {"CBA_762x39_AK","CBA_762x39_RPK"};
    };
    //M-21 Zastava
    class rhs_weap_m21_base: rhs_weap_m70_base
    {
        magazineWell[] = {"CBA_556x45_M21"}; //Inherits from rhs_weap_m70_base which is other calibre
    };
    //M-76 Zastava
    class rhs_weap_m76: rhs_weap_m70_base
    {
        magazineWell[] = {"CBA_792x57_M76"}; //Inherits from rhs_weap_m70_base which is other calibre
    };
    //M84
    class Rifle_Long_Base_F;
    class rhs_weap_m84: Rifle_Long_Base_F
    {
        magazineWell[] += {"CBA_762x54R_LINKS"};
    };
    //M84A Scorpion
    class SMG_01_F;
    class rhs_weap_scorpion: SMG_01_F
    {
        magazineWell[] = {"CBA_32ACP_Vz61"}; //Inherits from SMG_01_F which is other calibre
    };
    // M79
    class rhs_weap_m79: Rifle_Base_F {
        magazineWell[] += {"CBA_40mm_M203"};
    };
};
