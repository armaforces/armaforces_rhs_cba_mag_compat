function Get-Arma3-Dir {

    $registryPaths = @(
        'HKLM\SOFTWARE\Wow6432Node\Bohemia Interactive\ArmA 3',
        'HKLM\SOFTWARE\Wow6432Node\Bohemia Interactive Studio\ArmA 3',
        'HKLM\SOFTWARE\Bohemia Interactive Studio\ArmA 3',
        'HKLM\SOFTWARE\Bohemia Interactive\ArmA 3'
    )

    foreach ($path in $registryPaths) {
        try {
            $armaPath = Get-ItemProperty -Path Registry::$path -ErrorAction Stop
            return $armaPath.main
        } catch {
            Write-Debug "Arma 3 path not found in key: '$path'"
        }
    }

    Throw "Can't find Arma 3 path in registry"
}

function Invoke-ProcessWait {
    param (
        [Parameter(Mandatory=$true, Position=0)]
        [string] $FilePath,
        [Parameter(Mandatory=$false, Position=1)]
        [string[]] $ArgumentList = @()
    )

    $proc = Start-Process $FilePath $ArgumentList -PassThru
    $handle = $proc.Handle # cache proc.Handle
    $proc.WaitForExit();

    if ($proc.ExitCode -ne 0) {
        Throw "Process exited with status code $($proc.ExitCode)"
    }
}
