. "$PSScriptRoot\functions.ps1"

$arma3Dir = Get-Arma3-Dir
$arma3Exe = "$arma3Dir\arma3_x64.exe"
$arma3ModDir = "$arma3Dir\!Workshop"
$testedMod = $env:CI_JOB_NAME -replace "configdump_"
# Import autotest functions
. "$arma3Dir\autotest\functions.ps1"

$mods = @(
    "$arma3ModDir\@`CBA_A3",
    "$arma3ModDir\@`RHSAFRF",
    "$arma3ModDir\@`RHSUSAF",
    "$arma3ModDir\@`RHSGREF",
    "$arma3ModDir\@`RHSSAF"
)

New-Item -Path $env:CI_PROJECT_DIR -name dump -ItemType "directory"

# Autotest without Compat addon
$launchCommand = Get-Arma3-AutoTestCommand $arma3Dir 'dump_CfgWeapons' $mods
Write-Host $launchCommand
Write-Host (Get-Date -Format g)
Invoke-ProcessWait $launchCommand[0] -ArgumentList ($launchCommand | Select-Object -Skip 1)

Copy-Item "$arma3Dir\dump_CfgWeapons.cpp" "$env:CI_PROJECT_DIR\dump\$($testedMod)_no_compat.cpp"

#========================
Write-Host

$mods += "$env:CI_PROJECT_DIR\@armaforces_rhs_cba_mag_compat_$($testedMod)"

# Autotest with Compat addon
$launchCommand = Get-Arma3-AutoTestCommand $arma3Dir 'dump_CfgWeapons' $mods
Write-Host (Get-Date -Format g)
Write-Host $launchCommand
Invoke-ProcessWait $launchCommand[0] -ArgumentList ($launchCommand | Select-Object -Skip 1)

Copy-Item "$arma3Dir\dump_CfgWeapons.cpp" "$env:CI_PROJECT_DIR\dump\$($testedMod)_with_compat.cpp"

Write-Host
# Get Lines that were changed between dump and no dump
$changedLines = Get-GitChangedLines "$env:CI_PROJECT_DIR\dump\$($testedMod)_no_compat.cpp" "$env:CI_PROJECT_DIR\dump\$($testedMod)_with_compat.cpp"
Write-Host "================ LINES MODIFIED BY COMPAT DIFF ====================="
$valid = Out-GitChangesVerify $changedLines
Write-Host "==========================  END DIFF  =============================="

if (!$valid) {
    Write-Host "Invalid changes detected" -ForegroundColor Red
    Throw "Addon: $testedMod"
}
