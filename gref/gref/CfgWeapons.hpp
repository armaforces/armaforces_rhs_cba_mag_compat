class CfgWeapons
{
    class Rifle;
    class Rifle_Base_F;
    class Pistol_Base_F;

    //Izh-18
    class rhs_weap_Izh18: Rifle_Base_F
    {
        magazineWell[] += {"CBA_12g_1rnds"};
    };
    //Kar98k
    class rhs_weap_kar98k_Base_F: Rifle_Base_F
    {
        magazineWell[] += {"CBA_792x57_K98"};
    };
    //M-70, M-92 Zastava
    class rhs_weap_m70_base: Rifle_Base_F
    {
        magazineWell[] += {"CBA_762x39_AK","CBA_762x39_RPK"};
    };
    //M-21 Zastava
    class rhs_weap_m21_base: rhs_weap_m70_base
    {
        magazineWell[] = {"CBA_556x45_M21"}; //Inherits from rhs_weap_m70_base which is other calibre
    };
    //M-76 Zastava
    class rhs_weap_m76: rhs_weap_m70_base
    {
        magazineWell[] = {"CBA_792x57_M76"}; //Inherits from rhs_weap_m70_base which is other calibre
    };
    //M1 Garand
    class rhs_weap_M1garand_Base_F: Rifle_Base_F
    {
        magazineWell[] += {"CBA_3006_Garand"};
    };
    //M3 Grease Gun
    class rhs_weap_m3a1_base: Rifle_Base_F
    {
        magazineWell[] += {"CBA_45ACP_Grease"};
    };
    //MG42
    class rhs_weap_mg42_base: Rifle_Base_F
    {
        magazineWell[] += {"CBA_792x57_LINKS"};
    };
    //Mosin Nagant M38
    class rhs_weap_m38_Base_F: Rifle_Base_F
    {
        magazineWell[] += {"CBA_762x54R_Mosin"};
    };
    //MP44
    class rhs_weap_MP44_base: Rifle_Base_F
    {
        magazineWell[] += {"CBA_792x33_StG"};
    };
    //Sa vz. 58
    class rhs_weap_savz58_base: Rifle_Base_F
    {
        magazineWell[] += {"CBA_762x39_VZ58"};
    };
    //Sa vz. 61
    class SMG_01_F;
    class rhs_weap_savz61: SMG_01_F
    {
        magazineWell[] = {"CBA_32ACP_Vz61"};
    };
    //VHS
    class rhs_weap_vhs2_base: Rifle_Base_F
    {
        magazineWell[] = {"CBA_556x45_STANAG","CBA_556x45_G36"};
    };
};
