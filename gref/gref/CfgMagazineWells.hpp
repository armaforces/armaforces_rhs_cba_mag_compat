class CfgMagazineWells
{
    class CBA_12g_1rnds 
    {
        RHS_GREF_Mags[] =
        {
            //1 round 12 gauge shells (Izh-18)
            "rhsgref_1Rnd_00Buck",
            "rhsgref_1Rnd_Slug"
        };
    };
    class CBA_3006_Garand
    {
        RHS_GREF_Mags[] =
        {
            //8 round M1 Garand mags
            "rhsgref_8Rnd_762x63_M2B_M1rifle",
            "rhsgref_8Rnd_762x63_Tracer_M1T_M1rifle"
        };
    };
    class CBA_45ACP_Grease
    {
        RHS_GREF_Mags[] =
        {
            //30 round M3 Grease Gun mags
            "rhsgref_30rnd_1143x23_M1911B_SMG",
            "rhsgref_30rnd_1143x23_M1T_SMG",
            "rhsgref_30rnd_1143x23_M1911B_2mag_SMG",
            "rhsgref_30rnd_1143x23_M1T_2mag_SMG"
        };
    };
    class CBA_556x45_M21
    {
        RHS_GREF_Mags[] =
        {
            //30 round Zastava M21 mags
            "rhsgref_30rnd_556x45_m21",
            "rhsgref_30rnd_556x45_m21_t"
        };
    };
    class CBA_556x45_G36
    {
        RHS_GREF_Mags[] =
        {
            //30 round VHS-2 mags
            "rhsgref_30rnd_556x45_vhs2",
            "rhsgref_30rnd_556x45_vhs2_t"
        };
    };
    class CBA_762x39_AK
    {
        RHS_GREF_Mags[] =
        {
            //30 round Zastava M-70 mags
            "rhs_30Rnd_762x39mm",
            "rhs_30Rnd_762x39mm_tracer",
            "rhs_30Rnd_762x39mm_89",
            "rhs_30Rnd_762x39mm_U"
        };
    };
    class CBA_762x39_VZ58
    {
        RHS_GREF_Mags[] =
        {
            //30 round Sa vz. 58
            "rhs_30Rnd_762x39mm_Savz58",
            "rhs_30Rnd_762x39mm_Savz58_tracer"
        };
    };
    class CBA_762x54R_Mosin
    {
        RHS_GREF_Mags[] =
        {
            //5 round Mosin Nagant mag
            "rhsgref_5Rnd_762x54_m38"
        };
    };
    class CBA_32ACP_Vz61
    {
        RHS_GREF_Mags[] =
        {
            //20 round Sa vz. 61 mag
            "rhsgref_20rnd_765x17_vz61",
            //10 round Sa vz. 61 mag
            "rhsgref_10rnd_765x17_vz61"
        };
    };
    class CBA_792x33_StG
    {
        RHS_GREF_Mags[] =
        {
            //30 round MP44 mag
            "rhsgref_30Rnd_792x33_SmE_StG",
            //25 round MP44 mag
            "rhsgref_25Rnd_792x33_SmE_StG"
        };
    };
    class CBA_792x57_K98
    {
        RHS_GREF_Mags[] =
        {
            //5 round Kar98k mag
            "rhsgref_5Rnd_792x57_kar98k"
        };
    };
    class CBA_792x57_LINKS
    {
        RHS_GREF_Mags[] =
        {
            //50 round MG42 drums
            "rhsgref_50Rnd_792x57_SmE_drum",
            "rhsgref_50Rnd_792x57_SmE_notracers_drum",
            "rhsgref_50Rnd_792x57_SmK_drum",
            "rhsgref_50Rnd_792x57_SmK_alltracers_drum",
            //296 round MG42 belts
            "rhsgref_296Rnd_792x57_SmE_belt",
            "rhsgref_296Rnd_792x57_SmE_notracers_belt",
            "rhsgref_296Rnd_792x57_SmK_belt",
            "rhsgref_296Rnd_792x57_SmK_alltracers_belt"
        };
    };
    class CBA_792x57_M76
    {
        RHS_GREF_Mags[] =
        {
            //10 round M-76 mag
            "rhsgref_10Rnd_792x57_m76"
        };
    };
};
